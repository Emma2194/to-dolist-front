import React, { useState, useContext } from "react";
import { Button, Form, Grid, Header, Segment } from "semantic-ui-react";
import "../../assets/styles/login/styles.css";
import App_Context from "../../context/AppContext";
import { login } from "../../services/login";

const Login = ({ history }) => {
  const [user, setUser] = useState({ username: "", password: "" });

  const { AUTH } = useContext(App_Context);
  const handleOnChange = (e) => {
    setUser({
      ...user,
      [e.target.name]: e.target.value,
    });
  };
  const onClickLogin = async () => {
    let token = await login(user);
    if (token.token) {
      localStorage.setItem("x-token", token.token);
      AUTH.setAuth(token.user);
      history.push("/home");
    } else {
      alert("no permisos");
    }
  };

  return (
    <div className="backGround">
      <Grid
        textAlign="center"
        style={{ height: "100vh" }}
        verticalAlign="middle"
      >
        <Grid.Column style={{ maxWidth: 450 }}>
          <Header as="h2" color="blue" textAlign="center">
            My ToDo List
          </Header>
          <Form size="large">
            <Segment stacked>
              <Form.Input
                fluid
                icon="user"
                iconPosition="left"
                placeholder="Username"
                name="username"
                onChange={handleOnChange}
              />
              <Form.Input
                fluid
                icon="lock"
                iconPosition="left"
                placeholder="Password"
                type="password"
                name="password"
                onChange={handleOnChange}
              />

              <Button color="blue" fluid size="large" onClick={onClickLogin}>
                Login
              </Button>
            </Segment>
          </Form>
        </Grid.Column>
      </Grid>
    </div>
  );
};

export default Login;
