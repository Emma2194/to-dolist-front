import React, { useEffect, useState, useContext } from "react";
import {
  Button,
  Form,
  Grid,
  Header,
  Segment,
  Icon,
  Table,
  Checkbox,
} from "semantic-ui-react";
import { useTransition, animated } from "react-spring";
import {
  getTasks,
  addTask,
  removeTask,
  changeTask,
} from "../../services/tasks";
import App_Context from "../../context/AppContext";
import "../../assets/styles/home/styles.css";
import { renew } from "../../services/login";
import { hydrate } from "react-dom";

const Home = ({ history }) => {
  const { AUTH } = useContext(App_Context);
  const [isVisble, setIsVisible] = useState(false);
  const [edit, setEdit] = useState("");
  const [modified, setModified] = useState({
    name: "",
    done: false,
    uidUser: AUTH.auth.uid,
  });
  const [newTask, setNewTask] = useState({
    name: "",
    done: false,
    uidUser: AUTH.auth.uid,
  });
  const [tasks, setTasks] = useState([]);
  const [tasksFilter, setTasksFilter] = useState([]);
  const transition = useTransition(isVisble, {
    from: { x: 0, y: -50, opacity: 0 },
    enter: { x: 0, y: 0, opacity: 1 },
    leave: { x: 0, y: -50, opacity: 0 },
  });
  const handleOnChange = (e) => {
    setNewTask({
      ...newTask,
      [e.target.name]: e.target.value,
    });
  };
  const handleOnChangeEdit = (e) => {
    setModified({
      ...modified,
      [e.target.name]: e.target.value,
    });
  };
  const selectToUpdate = (task) => {
    setEdit(task.name);
    setModified({ ...task });
  };
  const createTask = async () => {
    try {
      await addTask(newTask);

      setNewTask({
        name: "",
        done: false,
        uidUser: AUTH.auth.uid,
      });
    } catch (error) {
      alert(error.response.data.msg);
    }

    let response = await getTasks(AUTH.auth.uid);
    setTasks(await response.data.tasks);
    setTasksFilter(await response.data.tasks);
  };
  const checked = async (task) => {
    task.done = !task.done;

    updateTask(task);
  };
  const updateTask = async (task) => {
    try {
      await changeTask(task.uid, task);
    } catch (error) {
      alert(error.response.data.msg);
    }

    let response = await getTasks(AUTH.auth.uid);
    setTasks(await response.data.tasks);
    setTasksFilter(await response.data.tasks);
  };
  const deleteTask = async (uid) => {
    await removeTask(uid);
    let response = await getTasks(AUTH.auth.uid);
    setTasks(await response.data.tasks);
    setTasksFilter(await response.data.tasks);
  };
  const taskFilter = (filter) => {
    if (filter == "ALL") {
      setTasksFilter([...tasks]);
    } else if (filter == "PENDING") {
      setTasksFilter([...tasks.filter((t) => t.done == false)]);
    } else {
      setTasksFilter([...tasks.filter((t) => t.done == true)]);
    }
  };
  useEffect(async () => {
    // if (!AUTH.auth.username) {
    try {
      let res = await renew();
      AUTH.setAuth(res.user);
      setIsVisible(true);
      let response = await getTasks(res.user.uid);
      setTasks(await response.data.tasks);
      setTasksFilter(await response.data.tasks);
    } catch (error) {
      history.push("/");
    }
    // }
  }, []);

  return (
    <div className="backGroundSoft">
      <Grid
        textAlign="center"
        style={{ height: "100vh" }}
        verticalAlign="middle"
      >
        <Grid.Column style={{ maxWidth: 700 }}>
          {transition((style, item) =>
            item ? (
              <animated.div style={style}>
                <Form size="large">
                  {transition}
                  <Segment>
                    <Header as="h2" color="blue" textAlign="center">
                      {AUTH.auth.username}'s List
                    </Header>
                    <Form.Input
                      action={{
                        icon: "add",
                        onClick: async () => await createTask(),
                      }}
                      value={newTask.name}
                      placeholder="New Task..."
                      onChange={handleOnChange}
                      name="name"
                    />
                  </Segment>

                  <Segment>
                    <Button.Group>
                      <Button onClick={() => taskFilter("ALL")}>All</Button>
                      <Button onClick={() => taskFilter("PENDING")}>
                        Pending
                      </Button>
                      <Button onClick={() => taskFilter("DONE")}>Done</Button>
                    </Button.Group>
                    {tasksFilter.length > 0 ? (
                      <Table selectable singleLine>
                        <Table.Header>
                          <Table.Row>
                            <Table.HeaderCell></Table.HeaderCell>
                            <Table.HeaderCell>Task</Table.HeaderCell>
                            <Table.HeaderCell>Options</Table.HeaderCell>
                          </Table.Row>
                        </Table.Header>

                        <Table.Body>
                          {tasksFilter.length > 0 &&
                            tasksFilter.map((t) =>
                              t.done ? (
                                <Table.Row className="done">
                                  <Table.Cell collapsing>
                                    <Checkbox
                                      checked={t.done}
                                      onClick={() => checked(t)}
                                    />
                                  </Table.Cell>
                                  {edit == t.name ? (
                                    <Form.Input
                                      action={{
                                        icon: "save",
                                        onClick: async () =>
                                          await updateTask(modified),
                                      }}
                                      value={modified.name}
                                      placeholder="New name..."
                                      onChange={handleOnChangeEdit}
                                      name="name"
                                    />
                                  ) : (
                                    <Table.Cell>{t.name}</Table.Cell>
                                  )}

                                  <Table.Cell collapsing>
                                    <Icon
                                      circular
                                      color="blue"
                                      name="pencil"
                                      onClick={() => selectToUpdate(t)}
                                    ></Icon>
                                    <Icon
                                      circular
                                      color="red"
                                      name="delete"
                                      onClick={() => deleteTask(t.uid)}
                                    ></Icon>
                                  </Table.Cell>
                                </Table.Row>
                              ) : (
                                <Table.Row>
                                  <Table.Cell collapsing>
                                    <Checkbox
                                      checked={t.done}
                                      onClick={() => checked(t)}
                                    />
                                  </Table.Cell>
                                  {edit == t.name ? (
                                    <Form.Input
                                      action={{
                                        icon: "save",
                                        onClick: async () =>
                                          await updateTask(modified),
                                      }}
                                      value={modified.name}
                                      placeholder="New name..."
                                      onChange={handleOnChangeEdit}
                                      name="name"
                                    />
                                  ) : (
                                    <Table.Cell>{t.name}</Table.Cell>
                                  )}

                                  <Table.Cell collapsing>
                                    <Icon
                                      circular
                                      color="blue"
                                      name="pencil"
                                      onClick={() => selectToUpdate(t)}
                                    ></Icon>
                                    <Icon
                                      circular
                                      color="red"
                                      name="delete"
                                      onClick={() => deleteTask(t.uid)}
                                    ></Icon>
                                  </Table.Cell>
                                </Table.Row>
                              )
                            )}
                        </Table.Body>
                      </Table>
                    ) : (
                      ""
                    )}
                  </Segment>
                </Form>
              </animated.div>
            ) : (
              ""
            )
          )}
        </Grid.Column>
      </Grid>
    </div>
  );
};

export default Home;
