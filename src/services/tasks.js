import axios from "axios";
import { PATH } from "./config.js";

async function getTasks(uidUser) {
  const header = {
    headers: {
      "x-token": localStorage.getItem("x-token"),
    },
  };
  const path = `${PATH}task/getByUid/${uidUser}`;
  return await axios.get(path, header);
}

async function addTask(task) {
  const header = {
    headers: {
      "x-token": localStorage.getItem("x-token"),
    },
  };
  const path = `${PATH}task/`;
  return await axios.post(path, task, header);
}

async function removeTask(uidUser) {
  const header = {
    headers: {
      "x-token": localStorage.getItem("x-token"),
    },
  };
  const path = `${PATH}task/delete/${uidUser}`;
  return await axios.put(path, null, header);
}

async function changeTask(uid, task) {
  const header = {
    headers: {
      "x-token": localStorage.getItem("x-token"),
    },
  };
  const path = `${PATH}task/update/${uid}`;
  return await axios.put(path, task, header);
}

export { getTasks, addTask, removeTask, changeTask };
