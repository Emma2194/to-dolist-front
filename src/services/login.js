import axios from "axios";
import { PATH } from "./config.js";

export async function login(user) {
  const res = await axios.post(`${PATH}login`, user);
  let token = res.data;
  return token;
}
export async function renew() {
  const header = {
    headers: {
      "x-token": localStorage.getItem("x-token"),
    },
  };
  const res = await axios.get(`${PATH}login/renew`, header);
  let token = res.data;
  return token;
}
