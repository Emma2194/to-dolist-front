import React from "react";

import Home from "../components/home/home";

function HomePage({ history }) {
  return (
    <div>
      <Home history={history} />
    </div>
  );
}

export default HomePage;
