import React from "react";

import Login from "../components/login/Login";

function LoginPage({ history }) {
  return (
    <div>
      <Login history={history} />
    </div>
  );
}

export default LoginPage;
