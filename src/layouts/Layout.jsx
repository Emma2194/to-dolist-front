import React, { useContext } from "react";
import App_Context from "../context/AppContext";

function layout({ children }) {
  const context = useContext(App_Context);

  return <div> {children} </div>;
}

export default layout;
