import React, { useState } from "react";
import { Switch, Route, BrowserRouter } from "react-router-dom";

import Layout from "../layouts/layout";
import App_Context from "../context/AppContext";
import Login from "../pages/login";
import Home from "../pages/home";

const App = () => {
  const [auth, setAuth] = useState({});

  const globalState = {
    AUTH: {
      auth,
      setAuth,
    },
  };

  return (
    <BrowserRouter>
      <App_Context.Provider value={globalState}>
        <Layout>
          <Switch>
            <Route exact path="/" component={Login} />
            <Route exact path="/home" component={Home} />
          </Switch>
        </Layout>
      </App_Context.Provider>
    </BrowserRouter>
  );
};

export default App;
